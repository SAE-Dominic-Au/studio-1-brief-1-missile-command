using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CommonActions : MonoBehaviour
{
    public int playSceneIndex;
    public GameObject optionsPanel;
    public bool inOptions;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (inOptions == true)
            {
                inOptions = false;
            }
            setActivePanels();
        }
    }
    // Start is called before the first frame update
    public void PlayButton()
    {
        SceneManager.LoadScene(playSceneIndex);
    }

    public void OptionsButton()
    {
        inOptions = true;
        setActivePanels();
    }

    public void OptionsCloseButton()
    {
        inOptions = false;
        setActivePanels();
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void setActivePanels()
    {
        optionsPanel.SetActive(inOptions);
    }
}
