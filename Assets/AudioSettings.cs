using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioSettings : MonoBehaviour
{
    public GameObject masterAudio;
    private Slider masterSlider;
    public GameObject musicAudio;
    private Slider musicSlider;
    public GameObject sfxAudio;
    private Slider sfxSlider;

    private float eulerNumber = 2.7182818284590452353602874713527f;

    public AudioMixer audioMixer;
    // Start is called before the first frame update

    void Start()
    {
        masterSlider = masterAudio.GetComponentInChildren<Slider>();
        float value1;
        audioMixer.GetFloat("MasterVol", out value1);
        masterSlider.value = Mathf.Pow(eulerNumber,(value1/20));

        musicSlider = musicAudio.GetComponentInChildren<Slider>();
        float value2;
        audioMixer.GetFloat("MusicVol", out value2);
        musicSlider.value = Mathf.Pow(eulerNumber, (value2 / 20));

        sfxSlider = sfxAudio.GetComponentInChildren<Slider>();
        float value3;
        audioMixer.GetFloat("SFXVol", out value3);
        sfxSlider.value = Mathf.Pow(eulerNumber, (value3 / 20));
    }

    // Update is called once per frame
    public void UpdateAudioSettings()
    {
        audioMixer.SetFloat("MasterVol", Mathf.Log(masterSlider.value) * 20);
        //audioMixer.SetFloat("MusicVol", Mathf.Log(musicSlider.value) * 20);
        //audioMixer.SetFloat("SFXVol", Mathf.Log(sfxSlider.value) * 20);
    }
}
