using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public delegate void OnLevelLoss();
    public delegate void OnLevelComplete();
    public delegate void OnBaseConverted();
    public delegate void UpdateUI();
    public delegate void AddScore(int amount);
    public delegate void PlayClip(AudioClip clip);

    public static OnLevelLoss onLevelLossEvent;
    public static OnLevelComplete onLevelCompleteEvent;
    public static OnBaseConverted onBaseConvertedEvent;
    public static UpdateUI updateUIEvent;
    public static AddScore addScoreEvent;
    public static PlayClip playClipEvent;
}
