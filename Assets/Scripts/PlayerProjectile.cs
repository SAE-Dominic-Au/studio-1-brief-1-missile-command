using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    public float speed;
    public Vector3 positionToMoveTowards;

    public string[] damageTag;
    public string[] impactTags = new string[] { "Env" };

    public GameObject playerExplosion;
    private bool runOnce = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, positionToMoveTowards, speed * Time.deltaTime);
        }

        else
        {
            transform.position = Vector2.MoveTowards(transform.position, transform.position, speed * Time.deltaTime);
        }

        if (Vector2.Distance(transform.position, positionToMoveTowards) < 0.1f) //reached destination
        {
            if(runOnce == false)
            {
                if (playerExplosion != null)
                {
                    Instantiate(playerExplosion, transform.position,Quaternion.identity);
                }
                this.GetComponent<SpriteRenderer>().color = Color.clear;
                float timeforTrail = this.GetComponent<TrailRenderer>().time;
                Destroy(this.gameObject, timeforTrail); //destroys trail as well

                runOnce = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CheckTag(damageTag, collision.tag))
        {
            if (playerExplosion != null)
            {
                if (runOnce == false)
                {
                    Instantiate(playerExplosion, transform.position, Quaternion.identity);
                    runOnce = true;
                }
            }

            EnemyScore es = collision.GetComponent<EnemyScore>();
            if (es != null)
            {
                es.AddScore();
            }

            positionToMoveTowards = transform.position;
            this.GetComponent<SpriteRenderer>().color = Color.clear;
            Destroy(collision.gameObject);
            float timeforTrail = this.GetComponent<TrailRenderer>().time;
            Destroy(this.gameObject, timeforTrail); //destroys trail as well
        }

        if (CheckTag(impactTags, collision.tag))
        {
            positionToMoveTowards = transform.position;
            this.GetComponent<SpriteRenderer>().color = Color.clear;
            float timeforTrail = this.GetComponent<TrailRenderer>().time;
            Destroy(this.gameObject, timeforTrail);
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;

        for (int i = 0; i < tagCollection.Length; i++)
        {
            if (tagCollection[i] == tagToCheck)
            {
                tagExists = true;
                break;
            }
        }

        return tagExists;
    }


}
