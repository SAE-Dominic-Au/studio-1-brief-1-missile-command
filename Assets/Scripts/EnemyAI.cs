using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public GameObject enemyProjectile;
    public float fireRate;
    private float counter;
    public Transform spawnLocation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (counter > 0)
        {
            counter -= Time.deltaTime;
        }

        if (counter <= 0)
        {
            GameObject temp = Instantiate(enemyProjectile, transform.position, Quaternion.identity);

            EnemyProjectile enemyProj = temp.GetComponent<EnemyProjectile>();
            if (enemyProj != null)
            {
                float newTargetX = Random.Range(-50, 50);

                enemyProj.positionToMoveTowards = new Vector3(newTargetX, -50, 0);
            }

            counter = fireRate;
        }
    }
}
