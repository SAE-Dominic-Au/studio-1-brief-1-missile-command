using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    public bool converted;
    public SpriteRenderer display;
    public bool runOnce = false;
    public Sprite abstractArt;
    public Sprite minimalistArt;
    // Start is called before the first frame update
    void Start()
    {
        converted = false;
        runOnce = false;
        display = this.transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy" && runOnce == false)
        {
            converted = true;
            display.sprite = abstractArt;
            display.drawMode = SpriteDrawMode.Sliced;
            display.transform.localScale = new Vector3(1, 2, 1);
            EventManager.onBaseConvertedEvent?.Invoke();
            runOnce = true;

        }
    }
}
