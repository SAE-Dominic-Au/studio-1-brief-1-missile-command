using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretFire : MonoBehaviour
{
    public GameObject playerProjectilePrefab;
    public Transform spawnLocation;
    public GameManager gameManager;
    public PlayerCrosshair playerCrosshair;

    // Start is called before the first frame update
    void Start()
    {
        playerCrosshair = FindObjectOfType<PlayerCrosshair>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.timeScale != 0)
        {
            if (Input.GetMouseButtonDown(0)) //0 is Left Click, Check using KeyCode.MouseX where X is the index
            {
                if(gameManager.ammoCount > 0)
                {
                    gameManager.ammoCount -= 1;
                    EventManager.updateUIEvent?.Invoke();
                    EventManager.playClipEvent?.Invoke(FindObjectOfType<AudioScript>().pew);

                    GameObject temp = Instantiate(playerProjectilePrefab, spawnLocation.position, Quaternion.identity);
                


                    PlayerProjectile playerProj = temp.GetComponent<PlayerProjectile>();
                    playerProj.positionToMoveTowards = playerCrosshair.transform.position;
                    temp.GetComponent<TrailRenderer>().time = (Vector2.Distance(spawnLocation.position, playerProj.positionToMoveTowards)) / playerProj.speed;
                }
            }
        }
    }
}
