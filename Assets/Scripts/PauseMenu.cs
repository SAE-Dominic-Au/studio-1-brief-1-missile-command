using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject optionsPanel;
    public bool isPaused;
    public bool inOptions;

    public int mainMenuIndex;
    // Start is called before the first frame update
    void Start()
    {
        if(pausePanel == null)
        {
            pausePanel = transform.GetChild(0).gameObject;
        }

        pausePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused == false)
            {
                isPaused = true;
                Time.timeScale = 0;
            }
            else
            {
                if(inOptions == true)
                {
                    inOptions = false;
                }
                else
                {
                    isPaused = false;
                    Time.timeScale = 1;
                }
            }

            setActivePanels();
        }
    }

    public void OptionsButton()
    {
        inOptions = true;
        setActivePanels();
    }

    public void OptionsCloseButton()
    {
        inOptions = false;
        setActivePanels();
    }

    public void ResumeButton()
    {
        isPaused = false;
        Time.timeScale = 1;
        setActivePanels();
    }

    public void QuitMenuButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(mainMenuIndex);
        Time.timeScale = 1;
    }
    public void setActivePanels()
    {
        pausePanel.SetActive(isPaused);
        optionsPanel.SetActive(inOptions);
    }
}
