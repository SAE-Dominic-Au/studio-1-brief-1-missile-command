using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public int ammoCount;
    public Base[] allBase;
    public int score;
    public int level;

    public int sceneIndex;
    public int endSceneIndex;
    public float waitDelay;

    // Start is called before the first frame update
    private void OnEnable()
    {
        EventManager.onLevelLossEvent += LevelRestart;
        EventManager.onLevelCompleteEvent += LevelComplete;
        EventManager.onBaseConvertedEvent += CheckBases;
        EventManager.addScoreEvent += AddScore;
    }
    private void OnDisable()
    {
        EventManager.onLevelLossEvent -= LevelRestart;
        EventManager.onLevelCompleteEvent -= LevelComplete;
        EventManager.onBaseConvertedEvent -= CheckBases;
        EventManager.addScoreEvent -= AddScore;

    }

    /*
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject);
    }
    */
    public void CheckBases()
    {
        bool allBasesConverted = true;

        for (int i = 0; i < allBase.Length; i++)
        {
            if (allBase[i].converted == false)
            {
                allBasesConverted = false;
                break;
            }
        }

        if (allBasesConverted == true)
        {
            EventManager.onLevelLossEvent?.Invoke();
        }
    }

    private void Start()
    {
        allBase = FindObjectsOfType<Base>();
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        endSceneIndex = SceneManager.sceneCountInBuildSettings - 1;
        score = PlayerPrefs.GetInt("currentScore", 0);
        level = PlayerPrefs.GetInt("level", 1);

        FindObjectOfType<EnemySkySpawner>().CountAmmo(level);
        EventManager.updateUIEvent?.Invoke();
    }
    /*
    private void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    */
    void LevelComplete()
    {

        PlayerPrefs.SetInt("currentScore", score);
        level += 1;
        while (sceneIndex > endSceneIndex)
        {
            sceneIndex -= 1;
        }
        //SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
        StartCoroutine(delayedLoadScene());
    }
    void LevelRestart()
    {
        PlayerPrefs.SetInt("currentScore", 0);
        level = 1;
        //SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
        StartCoroutine(delayedLoadScene());
    }

    IEnumerator delayedLoadScene()
    {
        PlayerPrefs.SetInt("level", level);
        Time.timeScale = 1;
        yield return new WaitForSeconds(waitDelay);
        SceneManager.LoadScene(sceneIndex);
        yield return new WaitForSeconds(Time.deltaTime);
        EventManager.updateUIEvent?.Invoke();
    }

    public void AddScore(int amount)
    {
        score += amount;
        PlayerPrefs.SetInt("currentScore", score);
    }
}
