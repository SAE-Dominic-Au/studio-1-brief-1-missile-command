using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerUI : MonoBehaviour
{
    public Text ammoText;
    public Text scoreText;
    public Text highscoreText;
    public Text levelText;

    private GameManager gm;

    private void OnEnable()
    {
        EventManager.updateUIEvent += UpdateUI;
    }

    private void OnDisable()
    {
        EventManager.updateUIEvent -= UpdateUI;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(ammoText == null)
        {
            ammoText = transform.GetChild(0).GetComponent<Text>();
        }

        if (scoreText == null)
        {
            scoreText = transform.GetChild(1).GetComponent<Text>();

        }

        if (highscoreText == null)
        {
            highscoreText = transform.GetChild(2).GetComponent<Text>();

        }

        if (levelText == null)
        {
            levelText = transform.GetChild(3).GetComponent<Text>();

        }

        gm = FindObjectOfType<GameManager>();

        UpdateUI();
    }

    public void UpdateUI()
    {
        ammoText.text = gm.ammoCount.ToString();
        //ammoText.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));

        scoreText.text = "Score: " + gm.score.ToString();

        highscoreText.text = "Highscore: " + PlayerPrefs.GetInt("highscore").ToString();

        levelText.text = "Level: " + (gm.level).ToString();
    }
}
