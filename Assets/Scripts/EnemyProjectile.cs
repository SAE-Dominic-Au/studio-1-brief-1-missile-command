using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float speed;
    public Vector3 positionToMoveTowards;
    public string[] despawnTags = new string[1]{"Env"};



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, positionToMoveTowards, speed * Time.deltaTime);
        }

        else
        {
            transform.position = Vector2.MoveTowards(transform.position, transform.position, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CheckTag(despawnTags, collision.tag))
        {
            Destroy(this.gameObject); //destroys trail as well
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;

        for (int i = 0; i < tagCollection.Length; i++)
        {
            if (tagCollection[i] == tagToCheck)
            {
                tagExists = true;
                break;
            }
        }

        return tagExists;
    }
}
