using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBumper : MonoBehaviour
{
    public string[] tagCollection;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CheckTag(tagCollection, collision.tag))
        {
            EnemyMove enemyMove = collision.GetComponent<EnemyMove>();
            if (enemyMove != null)
            {
                if (enemyMove.runOnce == false)
                {
                    enemyMove.direction *= -1;
                    enemyMove.runOnce = true;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (CheckTag(tagCollection, collision.tag))
        {
            EnemyMove enemyMove = collision.GetComponent<EnemyMove>();
            if (enemyMove != null)
            {
                enemyMove.runOnce = false;
            }
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;

        for (int i = 0; i < tagCollection.Length; i++)
        {
            if (tagCollection[i] == tagToCheck)
            {
                tagExists = true;
                break;
            }
        }

        return tagExists;
    }
}
