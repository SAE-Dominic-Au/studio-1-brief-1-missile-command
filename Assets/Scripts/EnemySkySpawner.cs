using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySkySpawner : MonoBehaviour
{
    public GameObject enemyProjectile;
    public int ammoCount;
    public int ammoPerLevel;
    public float fireRate;
    private float counter;

    public float spawnYlevel;
    public float minSpawnX;
    public float maxSpawnX;
    public bool sameX;
    public float sharedX;

    public bool runOnce = false;
    public bool outOfAmmo;
    public int remainingEnemies = 1;
    // Start is called before the first frame update
    void Start()
    {
        if(sameX == true)
        {
            float absX = Mathf.Abs(sharedX);
            minSpawnX = -absX;
            maxSpawnX = absX;
        }

        CountAmmo();
        outOfAmmo = false;
        runOnce = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (counter > 0)
        {
            counter -= Time.deltaTime;
        }

        if (counter <= 0)
        {
            if((ammoCount )> 0)
            {
                GameObject temp = Instantiate(enemyProjectile);
                float newX = Random.Range(minSpawnX, maxSpawnX);
                temp.transform.position = new Vector3(newX, spawnYlevel, 0);

                EnemyProjectile enemyProj = temp.GetComponent<EnemyProjectile>();
                if(enemyProj != null)
                {
                    float newTargetX = Random.Range(-50, 50);

                    enemyProj.positionToMoveTowards = new Vector3(newTargetX, -50, 0);
                }

                counter = fireRate;
                ammoCount--;
            }
        }

        if (ammoCount <= 0 && outOfAmmo == false)
        {
            outOfAmmo = true;
        }

        if(outOfAmmo == true)
        {
            remainingEnemies = 0;
            remainingEnemies += GameObject.FindGameObjectsWithTag("Enemy").Length;
            remainingEnemies += GameObject.FindGameObjectsWithTag("EnemyPlane").Length;
        }

        if (remainingEnemies == 0 && runOnce == false)
        {
            EventManager.onLevelCompleteEvent?.Invoke();
            Debug.Log("Level Won");
            remainingEnemies = 1;
            runOnce = true;
        }
    }

    public void CountAmmo(int level = -1)
    {
        if(level == -1)
        {
            level = FindObjectOfType<GameManager>().level;
        }
        ammoCount = ammoPerLevel * level;
    }
}
