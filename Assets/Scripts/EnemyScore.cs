using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScore : MonoBehaviour
{
    public int rewardScore = 100;
    private GameManager gameManager;

    public void AddScore()
    {
        EventManager.addScoreEvent?.Invoke(rewardScore);
        EventManager.updateUIEvent?.Invoke();
    }
}
