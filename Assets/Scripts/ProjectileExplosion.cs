using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileExplosion : MonoBehaviour
{
    public float growthSpeed;
    private float counter;
    public Vector3 initialScale;
    public float maxSize;

    public string[] damageTag;
    // Start is called before the first frame update
    void Start()
    {
        initialScale = transform.localScale;
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        //Vector3 newScale = transform.localScale;
        Vector3 newScale = new Vector3((initialScale.x + counter * growthSpeed), (initialScale.y + counter * growthSpeed),1);
        transform.localScale = newScale;

        if(newScale.x >= maxSize || newScale.y >= maxSize)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CheckTag(damageTag, collision.tag))
        {
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (CheckTag(damageTag, collision.tag))
        {
            Destroy(collision.gameObject);
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;

        for (int i = 0; i < tagCollection.Length; i++)
        {
            if (tagCollection[i] == tagToCheck)
            {
                tagExists = true;
                break;
            }
        }

        return tagExists;
    }
}
