using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    public AudioSource source;
    public AudioClip pew;

    private void OnEnable()
    {
        EventManager.playClipEvent += PlayAudioClip;
    }

    private void OnDisable()
    {
        EventManager.playClipEvent -= PlayAudioClip;
    }

    public void PlayAudioClip(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }
}
