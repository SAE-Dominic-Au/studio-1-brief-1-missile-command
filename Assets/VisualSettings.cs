using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualSettings : MonoBehaviour
{
    public Dropdown screenResolutionDropdown;
    public Toggle fullscreenToggle;
    // Start is called before the first frame update

    private void Start()
    {
        //UpdateVisualSettings();
    }
    // Update is called once per frame
    public void UpdateVisualSettings()
    {
        int resolution = screenResolutionDropdown.value;
        if(resolution == 0)
        {
            Screen.SetResolution(800, 800, fullscreenToggle.isOn);
        }
        else if (resolution == 1)
        {
            Screen.SetResolution(1000, 1000, fullscreenToggle.isOn);
        }
        else if (resolution == 2)
        {
            Screen.SetResolution(1200, 1200, fullscreenToggle.isOn);
        }
    }
}
